v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 160 -190 160 -170 {
lab=VSS}
N 160 -230 160 -210 {
lab=VSS}
N 160 -110 160 -90 {
lab=VSS}
N 100 -140 120 -140 {
lab=VSS}
N 160 -140 190 -140 {
lab=VSS}
N 100 -260 120 -260 {
lab=in}
N 160 -310 160 -290 {
lab=out}
N 160 -260 190 -260 {
lab=VSS}
N 160 -350 160 -330 {
lab=out}
N 100 -380 120 -380 {
lab=in}
N 160 -430 160 -410 {
lab=VDD}
N 160 -380 230 -380 {
lab=VDD}
N 190 -260 230 -260 {
lab=VSS}
N 190 -140 230 -140 {
lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 140 -260 0 0 {name=XN
w=100n
l=18n
nf=2
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 140 -380 0 0 {name=XP
w=100n
l=18n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 40 -510 0 0 {name=p1 lab=VDD}
C {devices/iopin.sym} 40 -490 0 0 {name=p2 lab=VSS}
C {devices/iopin.sym} 40 -460 0 0 {name=p3 lab=in}
C {devices/iopin.sym} 40 -440 0 0 {name=p4 lab=out}
C {devices/lab_pin.sym} 160 -90 0 0 {name=l1 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 100 -140 0 0 {name=l2 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 160 -190 0 0 {name=l3 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 230 -140 2 0 {name=l4 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 160 -210 2 0 {name=l5 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 230 -260 2 0 {name=l6 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 100 -260 0 0 {name=l7 sig_type=std_logic lab=in
}
C {devices/lab_pin.sym} 160 -310 2 0 {name=l8 sig_type=std_logic lab=out
}
C {devices/lab_pin.sym} 160 -330 0 0 {name=l9 sig_type=std_logic lab=out
}
C {devices/lab_pin.sym} 100 -380 0 0 {name=l10 sig_type=std_logic lab=in
}
C {devices/lab_pin.sym} 160 -430 2 0 {name=l11 sig_type=std_logic lab=VDD
}
C {devices/lab_pin.sym} 230 -380 2 0 {name=l12 sig_type=std_logic lab=VDD
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 140 -140 0 0 {name=XDUM
w=100n
l=18n
nf=4
model=nmos4_lvt
spiceprefix=X
}
